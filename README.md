# ttc_client.sh

## An (unofficial) helper script for the ESO Tamriel Trade Centre addon (for linux systems)

Provides a bash shell script

    ttc_client.sh

to emulate what the windows TTC client does that comes with the TTC addon.
It was written for ESO running on steam/proton on linux systems.
It may work with ESO running on other layers (eg by tweaking --eso-live-d, see below), but I don't know.

When started up, ttc_client.sh downloads the price table (as a zip file) and unzips it in the appropriate place.

It then monitors TamrielTradeCentre.lua in the ESO SavedVariables directory - if this changes, it uploads TamrielTradeCentre.lua via the form on the TTC web page
(https://ZONE.tamrieltradecentre.com/pc/Trade/WebClient (where ZONE='eu' or 'us')).
It uses the python3 script submit_to_ttc.py to do the upload.

### Prerequisites

If running Arch Linux, make a symlink:
```
    cd ~
    ln -s ~/.local/share/Steam .steam
```
so that steam files can be accessed under ~/.steam/ when running debian (files are under ~/.steam/ by default) and Arch Linux.

Packages

- python3
- chromium
- git
- curl
- unzip

are needed.

Under [Arch Linux](https://archlinux.org/), these can be installed with `sudo pacman -S --needed python3 chromium git curl unzip`

Arch Linux needs some additional packages, which the packages installed below on depend on. For python-selenium:

        sudo pacman -S --needed python-build python-installer python-setuptools python-pytest python-setuptools-rust python-wheel
        sudo pacman -S --needed python-urllib3 python-certifi python-trio python-trio-websocket python-websocket-client

Other distributions may not need these.

Finally,

- fswatch
- python-selenium
- selenium-manager

packages are needed. Under Arch Linux, these are [Arch User Repository (AUR) packages](https://aur.archlinux.org/packages) (ie contributed by users).
Refer to the [Arch User Repository wiki](https://wiki.archlinux.org/title/Arch_User_Repository) for
help on installing if needed. [Note: the current python-selenium version 4.24.0-1 seems broken. Use the previous version 4.22.0-1. In the python-selenium build directory run
`git checkout 65c77361b1b6621b4c4db06f1f97bc7bdd4c1f67` to check out this version before running `makepkg` etc.]

Some non-Arch Linux distributions may package the selenium python bindings ("python-selenium")
together with the selenium-manager executable. A final alternative is to use the pip3 python installer to install selenium (pip3 installs
both the bindings and the executable).

Arch Linux's chromium package includes `chromedriver` (needed to automate chromium).
Other distributions may need a separate package installing. Or download the linux64 chromedriver directly using
[https://googlechromelabs.github.io/chrome-for-testing/](https://googlechromelabs.github.io/chrome-for-testing/)
and move the chromedriver binary to somewhere in your PATH.

### Download

(First-time download) Run

    git clone https://gitlab.com/bike9876-eso/eso/ttc_client_linux

This will download files including bash script ttc_client.sh and python3 script submit_to_ttc.py
into directory ttc_client_linux relative to the current directory.

(Subsequent updates) Run

    cd /path/to/ttc_client
    git pull 

(replace `/path/to/ttc_client` with the actual path to ttc_client.sh.)

### Usage

If you use the EU ESO server, run

    /path/to/ttc_client.sh

(as above, replace `/path/to/ttc_client` with the actual path to ttc_client.sh.) Suggest you start this before starting up ESO.

You can give the option `--zone eu`, but the EU zone is the default, so there's no need. Use `--zone us` if you use the US ESO server.

This downloads the TTC price table, and monitors TamrielTradeCentre.lua as described above. Press Ctrl-c after finished playing to stop ttc_client.sh

After each upload of TamrielTradeCentre.lua, a list of the form:

```
5:31:55 PM: Item Sealed Jewelry Crafter Writ has been posted.
5:31:55 PM: Item Sealed Jewelry Crafter Writ has been posted.
5:31:55 PM: Item Sealed Jewelry Crafter Writ has been posted.
5:31:55 PM: Item Rawhide has been posted.
...
```

is output.

(An instance of chrome/chromium will also be fired temporarily up showing the same information.)

### License

This software is released under the MIT License. Please see LICENSE.

### Notes

- If you've recently run ttc_client.sh, there's no need to re-download the price table (it only changes once a day, I think). Run with option `--skip-download` to
skip downloading this.

- On the other hand, if you only want to download the price table (and not monitor TamrielTradeCentre.lua), use option `--skip-monitor`.

- The script assumes directory `eso_live_d` exists (default `~/.steam/steam/steamapps/compatdata/306130/pfx/drive_c/users/steamuser/My Documents/Elder Scrolls Online/live`).
If this directory is found elsewhere, use the --eso-live-d argument, eg

      ttc_client.sh --eso-live-d ~/drive_c/users/steamuser/My\ Documents/Elder\ Scrolls\ Online/live

    (Be careful if there are spaces in the path - escape them with a backslash.)

- The script catches the text "Upload Completed" in the output of the submission to https://ZONE.tamrieltradecentre.com/pc/Trade/WebClient. If you have a non English locale, your output might be different. Then use --upload-completed-text, eg --upload-completed-text "Téléchargement Terminé". However, I think the output of the webclient is currently always English, so this should not be an issue.

- This is an unofficial script and is not endorsed in any way by the authors of ESO or TTC.

[bike9876@エボ猫.コム](https://エボ猫.コム/)
