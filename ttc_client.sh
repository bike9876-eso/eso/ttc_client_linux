#!/bin/bash
# ttc_client.sh
# @bike9876 2022-04-29

progname=$(basename "$0")
usage="usage: $progname [-p | --platform pc|ps|xb (default pc)] [-z | --zone eu|us (default eu)] [--skip-download] [--skip-monitor] [--eso-live-d <eso_live_d>] [--upload-completed-text <upload_completed_text>] [--debug]"

eso_live_d=~/.steam/steam/steamapps/compatdata/306130/pfx/drive_c/users/steamuser/My\ Documents/Elder\ Scrolls\ Online/live
upload_completed_text='Upload Completed'

declare -A platforms_allowed=([pc]=1 [ps]=1 [xb]=1)
declare -A zones_allowed=([eu]=1 [us]=1)

function get_args() {
    if ! arg_parse=$(getopt -n $progname -o p:z:d -l platform:,zone:,eso-live-d:,upload-completed-text:,skip-download,skip-monitor,debug -- "$@"); then
        echo $usage >&2
        exit 1
    fi

    #echo $arg_parse

    eval set -- "$arg_parse"

    while true; do
        case "$1" in
            -p | --platform) platform=$2; shift 2 ;;
            -z | --zone) zone=$2; shift 2 ;;
            --skip-download) skip_download=1; shift ;;
            --skip-monitor) skip_monitor=1; shift ;;
            --eso-live-d) eso_live_d=$2; shift 2 ;;
            --upload-completed-text) upload_completed_text=$2; shift 2 ;;
            -d | --debug) debug=1; shift ;;
            --) shift; break ;;
            *) echo "Unexpected option: $1" >&2
                echo $usage >&2
                exit 1 ;;
        esac
    done

    if [[ -n $debug ]]; then
        echo "platform=$platform, zone=$zone, skip_download=$skip_download, skip_monitor=$skip_monitor, debug=$debug" >&2
    fi

    if [[ -z $platform || -z ${platforms_allowed[$platform]} ]]; then
        echo "$0: bad platform ('$platform')" >&2
        echo $usage >&2
        exit 1
    fi

    if [[ -z $zone || -z ${zones_allowed[$zone]} ]]; then
        echo "$0: bad zone ('$zone')" >&2
        echo $usage >&2
        exit 1
    fi

    if [[ ! -d $eso_live_d ]]; then
        echo "FATAL: ESO directory '$eso_live_d' not found." >&2
        echo "Maybe path of 'eso_live_d' in script needs fixing." >&2
        exit 1
    fi
}

function download_ttc_pricetable() {
    local fnname=${FUNCNAME[0]}
    local ttc_addons_d
    [[ -n $debug ]] && echo "$fnname" >&2

    ttc_addons_d="$eso_live_d/AddOns/TamrielTradeCentre"

    if [[ ! -d $ttc_addons_d ]]; then
        echo "FATAL: TTC addons directory '$ttc_addons_d' not found (has TTC addon been installed?)." >&2
        exit 1
    fi

    (
    cd "$ttc_addons_d"

    rm -f PriceTable.zip

    [[ -n $debug ]] && echo "will run 'curl -O --remote-header-name https://eu.tamrieltradecentre.com/download/PriceTable'" >&2
    echo "Downloading price table"
    curl -O --remote-header-name https://$zone.tamrieltradecentre.com/download/PriceTable

    echo "Unzipping price table"
    unzip -o PriceTable.zip
    echo
    )

    [[ -n $debug ]] && echo "$fnname done" >&2
}

function upload_if_changed() {
    local fnname=${FUNCNAME[0]}
    local md5_orig md5
    local ttc_file_path=$1
    local debugflag=${debug:+--debug}

    [[ -n $debug ]] && echo "$fnname" >&2

    while true; do
        # will be filename of updated file, as reported by fswatch
        read f
        [[ -n $debug ]] && echo "$fnname: $(date --iso-8601=s)" >&2
        [[ -n $debug ]] && echo "f=$f" >&2
        md5=$(md5sum "$f")
        [[ -n $debug ]] && echo "md5_orig=$md5_orig, md5=$md5" >&2
        if [[ -z $md5_orig || $md5 != $md5_orig ]]; then
            [[ -n $debug ]] && echo "submitting to ttc" >&2
            python3 submit_to_ttc.py --platform="$platform" --zone="$zone" --ttc_file_path="$ttc_file_path" --upload_completed_text="$upload_completed_text" $debugflag
            md5_orig=$md5
            echo
            echo "Monitoring $ttc_file_path..."
            echo "Press Ctrl-c to terminate."
        fi
    done
}

function monitor_and_upload_items_for_sale() {
    local fnname=${FUNCNAME[0]}
    local eso_saved_vars_d ttc_file_path

    [[ -n $debug ]] && echo "$fnname" >&2

    eso_saved_vars_d="$eso_live_d/SavedVariables"
    if [[ ! -d $eso_saved_vars_d ]]; then
        echo "FATAL: ESO SavedVariables directory '$eso_saved_vars_d' not found (has TTC addon been installed?)." >&2
        exit 1
    fi

    ttc_filename="TamrielTradeCentre.lua"
    ttc_filename_esc=$(echo "$ttc_filename" | sed -e 's/\./\\./g') # escape any '.'s
    ttc_file_path="$eso_saved_vars_d/$ttc_filename"

    echo "Monitoring $ttc_file_path..."
    echo "Press Ctrl-c to terminate."
    
    fswatch --event Updated "$eso_saved_vars_d" | grep --line-buffered "/$ttc_filename_esc$" | upload_if_changed "$ttc_file_path"
}

platform=pc
zone=eu
skip_download=
skip_monitor=
debug=

ttc_client_d=$(dirname "$0")
(
cd "$ttc_client_d"

get_args "$@"
[[ -z $skip_download ]] && download_ttc_pricetable
[[ -z $skip_monitor ]] && monitor_and_upload_items_for_sale
)
