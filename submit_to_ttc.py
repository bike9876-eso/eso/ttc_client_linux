# submit_to_ttc.py
# @bike9876 2022-04-29

# Submit the tamriel client addon's list of the ESO user's items for sale
# to the tamrieltradecentre.com site.

# Needs selenium python package installed.
# Needs chrome/chromium browser installed and in PATH (see options.binary_location below if not in PATH).
# Needs chrome webdriver installed and in PATH (see https://sites.google.com/chromium.org/driver/downloads for drivers) -
# unzip downloaded file and place 'chromedriver' in PATH.

import os
import sys
import argparse
import re
import time
from selenium import webdriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import selenium.common.exceptions

# Used if log network activity
import json
import pprint

zones_ok = ('eu','us')
platforms_ok = ('pc','ps','xb')

class TTCUpload():
    def __init__(self,args):
        self.args = args

        if self.args.zone not in zones_ok:
            raise ValueError('bad zone {} (must in one of {})'.format(self.args.zone,zones_ok))

        if self.args.platform not in platforms_ok:
            raise ValueError('bad platform {} (must in one of {})'.format(self.args.platform,platforms_ok))

    def dump_network_log(self):
        logs = self.driver.get_log('performance')
        log_msgs = [json.loads(logline['message'])['message'] for logline in logs]
        network_msgs = [msg for msg in log_msgs if re.match(r'Network',msg['method'])]

        with open(self.args.network_log_path, "wt") as out:
            pprint.pprint(network_msgs,stream=out)

    def setUp(self):
        if self.args.debug: print("setUp start",file=sys.stderr)

        options = webdriver.ChromeOptions()

        #headless = True
        headless = False

        # If a headless driver is used, by default a user-agent of including "HeadlessChrome" will be sent.
        # To avoid this, specify a user-agent.
        # If a non-headless driver is used, a normal "Chrome" user-agent will be sent (also a sec-ch-ua header is sent).
        # Easiest to then use the default user-agent.
        if headless:
            options.add_argument('--headless')
            options.add_argument('--user-agent=Mozilla/5.0 (X11; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0')

        # If need to specify path to chromium/chrome
        # options.binary_location = '/opt/google/chrome/google-chrome' # set if needed

        # log network (if debug set)
        if self.args.debug and self.args.network_log_path is not None:
            options.set_capability("goog:loggingPrefs", {'performance': 'ALL'})

        self.driver = webdriver.Chrome(options=options)

        if self.args.debug: print("setUp done",file=sys.stderr)

    def upload(self):
        if self.args.debug: print("upload start",file=sys.stderr)

        driver = self.driver

        ttc_url = self.args.ttc_url_template
        ttc_url = re.sub(r'{{ZONE}}',self.args.zone,ttc_url)
        ttc_url = re.sub(r'{{PLATFORM}}',self.args.platform,ttc_url)

        if self.args.debug: print('ttc_url = {}'.format(ttc_url),file=sys.stderr)

        driver.set_page_load_timeout(30)

        driver.get(ttc_url)

        if self.args.debug: print('ran driver.get({})'.format(ttc_url),file=sys.stderr)

        try:
            wait = WebDriverWait(driver,30);
            element = wait.until(expected_conditions.presence_of_element_located((By.XPATH,'//input[@name="SavedVarFileInput"]'))) # file upload input element on page
        except selenium.common.exceptions.TimeoutException as e:
            print("upload form timed out")
            return

        #print("page_source on initial load = ",driver.page_source)
        #print("element html = ",element.get_attribute('outerHTML'))

        if self.args.debug: print('ttc_file_path = {}'.format(self.args.ttc_file_path),file=sys.stderr)
        # Send the ttc_file_path to the input element
        element.send_keys(self.args.ttc_file_path)
        if self.args.debug: print('sent to input element',file=sys.stderr)

        try:
            wait = WebDriverWait(driver,30); # wait a max of the given number of secs
            web_client_console_panel = wait.until(expected_conditions.presence_of_element_located((By.XPATH,f'//div[@id="{self.args.web_client_console_panel_id}" and .//td[normalize-space()="{self.args.upload_completed_text}"]]')))
        except selenium.common.exceptions.TimeoutException as e:
            #print("page_source after upload = ",driver.page_source)
            print("upload timed out")
            return

        if self.args.debug: print('presence of {} found'.format(self.args.web_client_console_panel_id),file=sys.stderr)
        #if self.args.debug: print("page_source after upload = ",driver.page_source,file=sys.stderr)
        #if self.args.debug: print("web_client_console_panel = ",web_client_console_panel.get_attribute('outerHTML'),file=sys.stderr)

        if self.args.debug and self.args.network_log_path is not None:
            self.dump_network_log()

        tds = web_client_console_panel.find_elements(By.TAG_NAME,'td')

        time = None
        msg = None
        for td in tds:
            db = td.get_attribute('data-bind')
            if db == 'text: Time':
                time = td.text
            elif re.match(r'^text: Text(,|$)',db):
                msg = td.text
            if time is not None and msg is not None:
                print('{}: {}'.format(time,msg))
                time = None
                msg = None

        if self.args.debug: print("upload done",file=sys.stderr)

    def tearDown(self):
        if self.args.debug: print("tearDown start",file=sys.stderr)
        self.driver.close()
        if self.args.debug: print("tearDown done",file=sys.stderr)

#------------------------------------------------------------------------

parser = argparse.ArgumentParser(description='Upload ttc data to ttc server')
parser.add_argument('--zone',help='zone we are in {} (default: %(default)s)'.format(zones_ok),default='eu')
parser.add_argument('--platform',help='platform we are using {} (default: %(default)s)'.format(platforms_ok),default='pc')
parser.add_argument('--ttc_url_template',help='url template of ttc upload (default: %(default)s)',default='https://{{ZONE}}.tamrieltradecentre.com/{{PLATFORM}}/Trade/WebClient')
parser.add_argument('--ttc_file_path',help='path to ttc dump of items for sale (default: %(default)s)',default=os.environ['HOME']+"/.steam/steam/steamapps/compatdata/306130/pfx/drive_c/users/steamuser/My Documents/Elder Scrolls Online/live/SavedVariables/TamrielTradeCentre.lua")
parser.add_argument('--web_client_console_panel_id',help='id of web-client-console-panel (default: %(default)s)',default='web-client-console-panel')
parser.add_argument('--upload_completed_text',help='text to indicate upload completed (default: %(default)s)',default='Upload Completed')
parser.add_argument('--debug',help='output debugging output (including network log if --network_log_path is set)',action='store_true')
parser.add_argument('--network_log_path',help='path to network log (if --debug is set)',default=None)

args = parser.parse_args()

if args.debug: print('args=',args,file=sys.stderr)

ttc_upload = TTCUpload(args)
ttc_upload.setUp()
ttc_upload.upload()
ttc_upload.tearDown()
